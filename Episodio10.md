## Episodio 10

### Almacenamiento en caché

Se utiliza para trabajos muy pesados o que toman mucho tiempo al cargar

Editamos el archivo de las rutas *./routes/web.php*
Agregamos lo siguiente:
```php
  Route::get('posts/{post}', function ($slug) {
    if (! file_exists($path = __DIR__ . "/../resources/posts/{$slug}.html")) {
        redirect('/');
    }

    $post = cache() ->remember("posts.{$slug}", 1200, function () use ($path) {
        return file_get_contents($path);
    });

    return view('post', [
        'post' => $post
    ]);
  })->where('post', '[A-z_\-]+');
```

Los 1200 (segundos) corresponde al tiempo en que va a durar en cache el view. También se realiza una limpieza de código.