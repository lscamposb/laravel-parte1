## Episodio 9

### Restricciones de rutas

Editamos el archivo de las rutas *./routes/web.php*

Agregamos expresiones regulares que se pueden aceptar en la ruta
```php
  Route::get('posts/{post}', function ($slug) {
    $path = __DIR__ . "/../resources/posts/{$slug}.html";

    if (! file_exists($path)) {
        redirect('/');
    }

    $post = file_get_contents($path);

    return view('post', [
        'post' => $post
    ]);
  })->where('post', '[A-z_\-]+');
```