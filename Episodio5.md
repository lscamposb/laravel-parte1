## Episodio 5

### Route
Para agregar una ruta nueva se debe agregar en *./routes/web.php*

La ruta presente es la ruta principal del sitio web.
![Rutas](./image/route%20web.png)

Para agregar una ruta nueva se debe realizar de la siguiente forma

```bath
  Route::get('/', function () {
      return view('welcome');
  });

  Route::get('/home', function () {
      return view('welcome');
  });
```

![Rutas](./image/add%20route%20web.png)

Es la misma página pero si vemos la url cambia.
![Rutas](./image/add%20route%20url.png)

Si se agrega una ruta y no se especifica que un view, Laravel mostrara nada más un texto plano en la ruta del sitio web

```bath
  Route::get('/', function () {
    return view('welcome');
  });

  Route::get('/home', function () {
      return view('welcome');
  });

  Route::get('/hello', function () {
      return 'hello world!';
  });
```

![Rutas](./image/ruta_texto.png)