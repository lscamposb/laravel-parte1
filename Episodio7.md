## Episodio 7

### Nueva vista
Cambiamos el nombre del view welcome por posts.
![View](./image/renombrar%20view.png)

Agregar la vista *post.blade.php*
![View](./image/nueva%20vista.png)

Agregar en la vista
```html
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <script src="/app.js"></script>
    <body>
      <article>
        <h1>My First Post</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, dolorem. Sint corporis, placeat necessitatibus minus qui, ad perspiciatis enim adipisci facilis architecto hic voluptas autem odit unde molestiae, quidem culpa.
        </p>
      </article>

      <a href="/"> Go Back </a>
    </body>
  </html>
```

Editar las rutas
```php
  Route::get('/', function () {
      return view('posts');
  });

  Route::get('post', function () {
      return view('post');
  });
```

Editar el archivo de las rutas para que apunte al view renombrado.
![View](./image/ruta%20renombreda.png)

Agregamos estilos en el css
```css
  body {
      background: white;
      color: #222222;
      max-width: 600px;
      margin: auto;
      font-family: sans-serif;
  }

  p {
      line-height: 1.6;
  }

  article + article {
      margin-top: 3rem;
      padding-top: 3rem;
      border-top: 1px solid #c5c5c5;
  }
```

Agregamos 3 textos en la página y le agregamos referencias a los títulos de cada texto.
```html
  <article>
    <h1><a href="/post"> My First Post </a></h1>
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, dolorem. Sint corporis, placeat necessitatibus minus qui, ad perspiciatis enim adipisci facilis architecto hic voluptas autem odit unde molestiae, quidem culpa.
    </p>
  </article>

  <article>
    <h1><a href="/post"> My Second Post </a></h1>
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, dolorem. Sint corporis, placeat necessitatibus minus qui, ad perspiciatis enim adipisci facilis architecto hic voluptas autem odit unde molestiae, quidem culpa.
    </p>
  </article>

  <article>
    <h1><a href="/post"> My Third Post </a></h1>
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, dolorem. Sint corporis, placeat necessitatibus minus qui, ad perspiciatis enim adipisci facilis architecto hic voluptas autem odit unde molestiae, quidem culpa.
    </p>
  </article>
```
![View](./image/titulos%20referencias.png)
