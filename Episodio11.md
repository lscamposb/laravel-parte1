## Episodio 11

### Uso de la clase Filesystem

Utilizada para cargar el contenido de forma dinámica.

Vamos a crear un modelo para centralizar las rutas de post

Creamos el modelo en *./app/Models* con el nombre de *Post.php*
![View](./image/modelo%20post.png)

En el modelo creado agregamos
```php
  <?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Support\Facades\File;


    class Post {
      public static function find($slug) {
        base_path();

        if (! file_exists($path = resource_path("posts/{$slug}.html"))) {
            throw new ModelNotFoundException();        
        }

        return cache() ->remember("posts.{$slug}", 1200, function () use ($path) {
            return file_get_contents($path);
        });
      }

      public static function all() {
        $files = File ::files(resource_path("posts/"));

        return array_map(fn($file) => $file->getContents(), $files);
      }
    }
```

Editamos el archivo de las rutas *./routes/web.php*
```php
  Route::get('/', function () {
    return view('posts', [
        'posts' => Post::all()
    ]);
  });
```

Editamos el archivo *./views/post.blade.php* para que cagué los datos de forma dinámica
```html
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <script src="/app.js"></script>
    <body>
      <article>
          <?= $post; ?>
      </article>

      <a href="/"> Go Back </a>
    </body>
  </html>
```