## Episodio 12

### Paquete para publicar metadatos

Instalar el paquete Yaml 
```bath
    composer require spatie/yaml-front-matter
```

Editamos el archivo de las rutas *./routes/web.php*
```php
  Route::get('/', function () {
    return view('posts', [
        'posts' => Post::all()
    ]);
  });

  Route::get('posts/{post}', function ($slug) {
      $post = Post::find($slug);

      return view('post', [
          'post' => $post
      ]);    
  })->where('post', '[A-z_\-]+');
```

Editamos el archivo de las rutas *./app/Models/Post.php*
```php
  <?php
    namespace App\Models;

    use Illuminate\Support\Facades\File;
    use Spatie\YamlFrontMatter\YamlFrontMatter;

    class Post {
      public $title;
      public $excerpt;
      public $date;
      public $body;
      public $slug;

      public function __construct($title, $excerpt, $date, $body, $slug) {
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
        $this->slug = $slug;
      }

      public static function find($slug) {
        return static::all()->firstWhere('slug', $slug);
      }

      public static function all() {
        return collect(File::files(resource_path("posts")))
        ->map(function ($file){
            return YamlFrontMatter::parseFile($file);
        })
        ->map(function ($document) {
            return new Post(
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body(),
                $document->slug
            );
        });
      }
    }
```

Editamos el archivo de las views *./views

post.blade.php
```php
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <script src="/app.js"></script>
    <body>
      <article>
          <h1><?= $post->title; ?></h1>

          <div>
            <?= $post->body; ?>
          </div>
      </article>

      <a href="/"> Go Back </a>
    </body>
  </html>
```

posts.blade.php
```php
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <script src="/app.js"></script>
    <body>
      <?php foreach ($posts as $post) : ?>
        <article>
          <h1>
            <a href="/posts/<?= $post->slug; ?>">
              <?= $post->title; ?>
            </a>
          </h1>
          
          <div>
            <?= $post->excerpt; ?>
          </div>
        </article>
      <?php endforeach; ?>
    </body>
  </html>
```