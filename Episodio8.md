## Episodio 8

### Parametros entre views

Editar el archivo de las ruta *post* para agregar parametro

Ruta
```php
  Route::get('/', function () {
    return view('posts');
  });

  Route::get('posts/{post}', function ($slug) {
      $path = __DIR__ . "/../resources/posts/{$slug}.html";

      if (! file_exists($path)) {
          redirect('/');
      }

      $post = file_get_contents($path);

      return view('post', [
          'post' => $post
      ]);
  });
```

HTML
```html
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <script src="/app.js"></script>
    <body>
      <article>
          <?= $post; ?>
      </article>

      <a href="/"> Go Back </a>
    </body>
  </html>
```

En *./resources* agregamos una carpeta *posts*
![View](./image/resources.png)

En *./resources/posts* creamos un archivo *my-first-post.html*
```html
  <h1>My First Post</h1>

  <p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Et totam veniam dolorem. Dignissimos hic maxime modi nesciunt nobis sit libero ipsa odit, facilis minus autem a quae id consequatur amet.
  </p>
```
![View](./image/post%20html.png)

En *./resources/posts* creamos un archivo *my-first-second.html*
```html
  <h1>My Second Post</h1>

  <p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Et totam veniam dolorem. Dignissimos hic maxime modi nesciunt nobis sit libero ipsa odit, facilis minus autem a quae id consequatur amet.
  </p>
```
![View](./image/post%20html.png)

En *./resources/posts* creamos un archivo *my-first-third.html*
```html
  <h1>My Third Post</h1>

  <p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Et totam veniam dolorem. Dignissimos hic maxime modi nesciunt nobis sit libero ipsa odit, facilis minus autem a quae id consequatur amet.
  </p>
```
![View](./image/post%20html.png)

Editar el archivo *./resources/views/post.blade.php* y agregar el parametro que se va a enviar
```html
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <script src="/app.js"></script>
    <body>
      <article>
        <h1><a href="/posts/my-first-post"> My First Post </a></h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, dolorem. Sint corporis, placeat necessitatibus minus qui, ad perspiciatis enim adipisci facilis architecto hic voluptas autem odit unde molestiae, quidem culpa.
        </p>
      </article>

      <article>
        <h1><a href="/posts/my-second-post"> My Second Post </a></h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, dolorem. Sint corporis, placeat necessitatibus minus qui, ad perspiciatis enim adipisci facilis architecto hic voluptas autem odit unde molestiae, quidem culpa.
        </p>
      </article>

      <article>
        <h1><a href="/posts/my-thrid-post"> My Third Post </a></h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, dolorem. Sint corporis, placeat necessitatibus minus qui, ad perspiciatis enim adipisci facilis architecto hic voluptas autem odit unde molestiae, quidem culpa.
        </p>
      </article>
    </body>
  </html>
```