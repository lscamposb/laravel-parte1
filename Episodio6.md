## Episodio 6

### CSS and JavaScript

Vamos a editar el archivo *./resources/views/welcome.blade.php*

Eliminamos todo el código para realizarlo desde cero.
```html
  <!DOCTYPE html>
    <title>My Blog</title>
    <body>
      <h1>Hello World</h1>
    </body>
  </html>
```

#### CSS
Vamos crear un archivo en *./public/app.css* para tener el css de la página.
![Css](./image/archivo%20css.png)

Adentro de archivo agregamos el css del body

```css
  body {
    background: navy;
    color: white;
  }
```

Para ver reflejado los estilos del css debemos importarlo en el html
```html
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <body>
      <h1>Hello World</h1>
    </body>
  </html>
```
![Rutas](./image/css%20body%20azul.png)

#### JavaScript
Vamos crear un archivo en *./public/app.js* para las funciones de JavaScript de la página.
![JavaScript](./image/archivo%20js.png)

Adentro de archivo agregamos el css del body

```js
  alert('I am here!!');
```

Para utilizar las funciones del js debemos importarlo en el html
```html
  <!DOCTYPE html>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
    <script src="/app.js"></script>
    <body>
      <h1>Hello World</h1>
    </body>
  </html>
```

Al recargar la página nos va a mostrar el mensaje que colocamos en el alert del js.
![alert](./image/mensaje%20alert.png)